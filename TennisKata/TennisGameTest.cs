using NUnit.Framework;

namespace TennisKata_20210119
{
    
    public class TennisKataTest
    {
        private TennisGame _tennisGame;

        [SetUp]
        public void SetUp()
        {
            _tennisGame = new TennisGame();
        }

        #region 0:0 Love All
        /// <summary>
        /// 0:0 Love All
        /// </summary>
        [Test]
        public void When_game_start_be_love_all()
        {
            ScoreShouldBe("Love All");
        }
        #endregion

        #region 1:0 Fifteen Love
        /// <summary>
        /// 1:0 Fifteen Love
        /// </summary>
        [Test]
        public void Fifteen_Love()
        {
            GivenFirstPlayerScoreTimes(1);
            ScoreShouldBe("Fifteen Love");
        }
        #endregion

        #region 0:1 Love Fifteen
        /// <summary>
        /// 0:1 Love Fifteen
        /// </summary>
        [Test]
        public void Love_Fifteen()
        {
            GivenSecondPlayerScoreTimes(1);
            ScoreShouldBe("Love Fifteen");
        }
        #endregion

        #region 1:1 Fifteen All
        /// <summary>
        /// 1:1 Fifteen All
        /// </summary>
        [Test]
        public void Fifteen_All()
        {
            GivenFirstPlayerScoreTimes(1);
            GivenSecondPlayerScoreTimes(1);
            ScoreShouldBe("Fifteen All");
        }
        #endregion

        #region 2:0 Thirty Love
        /// <summary>
        /// 2:0 Thirty Love
        /// </summary>
        [Test]
        public void Thirty_Love()
        {
            GivenFirstPlayerScoreTimes(2);
            ScoreShouldBe("Thirty Love");
        }
        #endregion

        #region 0:2 Love Thirty
        /// <summary>
        /// 0:2 Love Thirty
        /// </summary>
        [Test]
        public void Love_Thirty()
        {
            GivenSecondPlayerScoreTimes(2);
            ScoreShouldBe("Love Thirty");
        }
        #endregion

        #region 2:1 Thirty Fifteen
        /// <summary>
        /// 2:1 Thirty Fifteen
        /// </summary>
        [Test]
        public void Thirty_Fifteen()
        {
            GivenFirstPlayerScoreTimes(2);
            GivenSecondPlayerScoreTimes(1);
            ScoreShouldBe("Thirty Fifteen");
        }
        #endregion

        #region 1:2 Fifteen Thirty
        /// <summary>
        /// 1:2 Fifteen Thirty
        /// </summary>
        [Test]
        public void Fifteen_Thirty()
        {
            GivenFirstPlayerScoreTimes(1);
            GivenSecondPlayerScoreTimes(2);
            ScoreShouldBe("Fifteen Thirty");
        }
        #endregion

        #region 2:2 Thirty All
        /// <summary>
        /// 2:2 Thirty All
        /// </summary>
        [Test]
        public void Thirty_All()
        {
            GivenFirstPlayerScoreTimes(2);
            GivenSecondPlayerScoreTimes(2);
            ScoreShouldBe("Thirty All");
        }
        #endregion

        #region 3:0 Forty Love
        /// <summary>
        /// 3:0 Forty Love
        /// </summary>
        [Test]
        public void Forty_Love()
        {
            GivenFirstPlayerScoreTimes(3);
            ScoreShouldBe("Forty Love");
        }
        #endregion

        #region 0:3 Love Forty
        /// <summary>
        /// 0:3 Love Forty
        /// </summary>
        [Test]
        public void Love_Forty()
        {
            GivenSecondPlayerScoreTimes(3);
            ScoreShouldBe("Love Forty");
        }
        #endregion

        #region 1:3 Fifteen Forty
        /// <summary>
        /// 1:3 Fifteen Forty
        /// </summary>
        [Test]
        public void Fifteen_Forty()
        {
            GivenFirstPlayerScoreTimes(1);
            GivenSecondPlayerScoreTimes(3);
            ScoreShouldBe("Fifteen Forty");
        }
        #endregion

        #region 2:3 Thirty Forty
        /// <summary>
        /// 2:3 Thirty Forty
        /// </summary>
        [Test]
        public void Thirty_Forty()
        {
            GivenFirstPlayerScoreTimes(2);
            GivenSecondPlayerScoreTimes(3);
            ScoreShouldBe("Thirty Forty");
        }
        #endregion

        #region 3:1 Forty Fifteen
        /// <summary>
        /// 3:1 Forty Fifteen
        /// </summary>
        [Test]
        public void Forty_Fifteen()
        {
            GivenFirstPlayerScoreTimes(3);
            GivenSecondPlayerScoreTimes(1);
            ScoreShouldBe("Forty Fifteen");
        }
        #endregion

        #region 3:2 Forty Thirty
        /// <summary>
        /// 3:2 Forty Thirty
        /// </summary>
        [Test]
        public void Forty_Thirty()
        {
            GivenFirstPlayerScoreTimes(3);
            GivenSecondPlayerScoreTimes(2);
            ScoreShouldBe("Forty Thirty");
        }
        #endregion

        #region Deuce
        /// <summary>
        /// 3:3 Deuce
        /// </summary>
        [Test]
        public void Deuce()
        {
            GivenFirstPlayerScoreTimes(3);
            GivenSecondPlayerScoreTimes(3);
            ScoreShouldBe("Deuce");
        }
        /// <summary>
        /// 4:4 Deuce
        /// </summary>
        [Test]
        public void Deuce4_4()
        {
            GivenFirstPlayerScoreTimes(4);
            GivenSecondPlayerScoreTimes(4);
            ScoreShouldBe("Deuce");
        }
        #endregion

        #region First Player Win
        /// <summary>
        /// 4:0 First Player Win
        /// </summary>
        [Test]
        public void FirstPlayerWin_4_0()
        {
            GivenFirstPlayerScoreTimes(4);
            GivenSecondPlayerScoreTimes(0);
            ScoreShouldBe("First Player Win");
        }
        /// <summary>
        /// 4:1 First Player Win
        /// </summary>
        [Test]
        public void FirstPlayerWin_4_1()
        {
            GivenFirstPlayerScoreTimes(4);
            GivenSecondPlayerScoreTimes(1);
            ScoreShouldBe("First Player Win");
        }
        /// <summary>
        /// 4:2 First Player Win
        /// </summary>
        [Test]
        public void FirstPlayerWin_4_2()
        {
            GivenFirstPlayerScoreTimes(4);
            GivenSecondPlayerScoreTimes(2);
            ScoreShouldBe("First Player Win");
        }
        /// <summary>
        /// 5:3 First Player Win
        /// </summary>
        [Test]
        public void FirstPlayerWin_5_3()
        {
            GivenFirstPlayerScoreTimes(5);
            GivenSecondPlayerScoreTimes(3);
            ScoreShouldBe("First Player Win");
        }
        #endregion

        #region Second Player Win
        /// <summary>
        /// 4:0 First Player Win
        /// </summary>
        [Test]
        public void SecondPlayerWin_0_4()
        {
            GivenFirstPlayerScoreTimes(0);
            GivenSecondPlayerScoreTimes(4);
            ScoreShouldBe("Second Player Win");
        }
        /// <summary>
        /// 4:1 First Player Win
        /// </summary>
        [Test]
        public void SecondPlayerWin_1_4()
        {
            GivenFirstPlayerScoreTimes(1);
            GivenSecondPlayerScoreTimes(4);
            ScoreShouldBe("Second Player Win");
        }
        /// <summary>
        /// 4:2 First Player Win
        /// </summary>
        [Test]
        public void SecondPlayerWin_2_4()
        {
            GivenFirstPlayerScoreTimes(2);
            GivenSecondPlayerScoreTimes(4);
            ScoreShouldBe("Second Player Win");
        }
        /// <summary>
        /// 5:3 First Player Win
        /// </summary>
        [Test]
        public void SecondPlayerWin_3_5()
        {
            GivenFirstPlayerScoreTimes(3);
            GivenSecondPlayerScoreTimes(5);
            ScoreShouldBe("Second Player Win");
        }
        #endregion

        #region First Player Advantage
        /// <summary>
        /// 4:3 First Player Advantage
        /// </summary>
        [Test]
        public void FirstPlayerAdvantage()
        {
            GivenFirstPlayerScoreTimes(4);
            GivenSecondPlayerScoreTimes(3);
            ScoreShouldBe("First Player Advantage");
        }
        #endregion

        #region Second Player Advantage
        /// <summary>
        /// 3:4 Second Player Advantage
        /// </summary>
        [Test]
        public void SecondPlayerAdvantage()
        {
            GivenFirstPlayerScoreTimes(3);
            GivenSecondPlayerScoreTimes(4);
            ScoreShouldBe("Second Player Advantage");
        }
        #endregion

        [Test]
        public void Test()
        {
            GivenFirstPlayerScoreTimes(6);
            GivenSecondPlayerScoreTimes(6);
            ScoreShouldBe("Deuce");
        }

        private void GivenFirstPlayerScoreTimes(int times)
        {
            for(int i=0;i<times;i++)
            {
                _tennisGame.FirstPlayerScore();
            }
        }

        private void GivenSecondPlayerScoreTimes(int times)
        {
            for(int i=0;i<times;i++)
            {
                _tennisGame.SecondPlayerScore();
            }
        }

        private void ScoreShouldBe(string expected)
        {
            Assert.AreEqual(expected, _tennisGame.Score());
        }
    }
}