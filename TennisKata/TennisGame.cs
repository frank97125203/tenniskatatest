﻿using System;
using System.Collections.Generic;

namespace TennisKata_20210119
{
    public class TennisGame
    {
        private int _firstPlayerScoreTimes;
        private int _secondPlayerScoreTimes;
        private readonly Dictionary<int, string> _scrosLoopup = new Dictionary<int, string>()
        {
            { 0, "Love" },
            { 1, "Fifteen" },
            { 2, "Thirty" },
            { 3, "Forty" }
        };
        private readonly Dictionary<bool, string> _playerName = new Dictionary<bool, string>()
        {
            { true, "First Player"},
            { false, "Second Player" }
        };

        public string Score()
        {
            if (WhenFirstPlayerScoreEqualSecondPlayer())
                switch (_firstPlayerScoreTimes)
                {
                    case 0:
                    case 1:
                    case 2:
                        return $"{_scrosLoopup[this._secondPlayerScoreTimes]} All";
                    default:
                        return "Deuce";
                }
            if (WhenAnyScoreLessThan4())
                return $"{_scrosLoopup[this._firstPlayerScoreTimes]} {_scrosLoopup[this._secondPlayerScoreTimes]}";
            if (WhenAnyScoreMoreThan3())
                return $"{_playerName[Who()]} {WinOrAdvantage()}";
            return "Love All";
        }

        private bool WhenFirstPlayerScoreEqualSecondPlayer()
        {
            return this._firstPlayerScoreTimes == this._secondPlayerScoreTimes;
        }
        private bool WhenAnyScoreLessThan4()
        {
            return this._firstPlayerScoreTimes < 4 && 
                this._firstPlayerScoreTimes >= 0 &&
                this._secondPlayerScoreTimes < 4 && 
                this._secondPlayerScoreTimes >= 0;
        }
        private bool WhenAnyScoreMoreThan3()
        {
            return this._firstPlayerScoreTimes > 3 || this._secondPlayerScoreTimes > 3;
        }
        private bool Who()
        {
            return this._firstPlayerScoreTimes - this._secondPlayerScoreTimes > 0;
        }
        private string WinOrAdvantage()
        {
            switch(this._firstPlayerScoreTimes - this._secondPlayerScoreTimes)
            {
                case 1:
                case -1:
                    return "Advantage";
                default:
                    return "Win";
            }
        }
        public void FirstPlayerScore()
        {
            this._firstPlayerScoreTimes++;
        }

        public void SecondPlayerScore()
        {
            this._secondPlayerScoreTimes++;
        }
    }
}